<?php

define('IVERI_LITE_SUBMISSION_ENDPOINT', 'https://backoffice.host.iveri.com/Lite/Transactions/New/Authorise.aspx');
define('IVERI_LITE_AUTH_INFO_ENDPOINT', 'https://backoffice.host.iveri.com/Lite/Transactions/New/AuthoriseInfo.aspx');
define('IVERI_CURRENCY_CODE','USD');

/**
 * Implements hook_menu().
 */
function commerce_iveri_lite_menu() {
  $items = array();

  $items['iveri/lite/back/ok'] = array(
    'title' => 'User Redirect after order complete - OK (ok)',
    'page callback' => 'commerce_iveri_lite_iver_callback_ok',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['iveri/lite/back/nok'] = array(
    'title' => 'User Redirect after order complete - NOK (not ok)',
    'page callback' => 'commerce_iveri_lite_iver_callback_nok',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['iveri/lite/back/tok'] = array(
    'title' => 'User Redirect after order complete - TOK (tryagain ok)',
    'page callback' => 'commerce_iveri_lite_iver_callback_tok',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  $items['iveri/lite/back/eok'] = array(
    'title' => 'User Redirect after order complete - EOK (error ok)',
    'page callback' => 'commerce_iveri_lite_iver_callback_eok',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;
}

/**
 * Callback from iVeri - OK
 */
function commerce_iveri_lite_iver_callback_ok() {
	$order_id = $_POST['ECOM_CONSUMERORDERID'];
	$transaction_index = $_POST['LITE_TRANSACTIONINDEX'];
	$payment_id = $_POST['DC_PAYMENT_ID'];
	$commerce_transaction_id = $_POST['DC_TRANSACTION_ID'];
	$auth_type = 'DIRECT_PAYMENT_CARD_STATUS_' . $_POST['DIRECT_PAYMENT_CARD_STATUS'];
	$amount = $_POST['LITE_ORDER_AMOUNT'];
	
	watchdog('commerce_iveri_lite', 'Receiving payment response for order !order_id from iVeri Lite', array('!order_id' => $order_id));

	$message = '';

	// Validate the response
	$order = commerce_order_load($order_id);
	$payment_method = commerce_payment_method_instance_load($payment_id);
	if ($payment_method && $transaction_index) {
		$auth_successful = TRUE;
	}
	else {
		$auth_successful = FALSE;
	}

	if (!$auth_successful || !$order) { // invalid response
		$message = t("We're sorry, but due to a technical problem, your order could not be processed. Please contact our help center.");
		watchdog('commerce_iveri_lite', 'An invalid response from has been received!', array() , WATCHDOG_ERROR);

	} else {
		$wrapper = entity_metadata_wrapper('commerce_order', $order);
		$quantity = commerce_line_items_quantity($wrapper->commerce_line_items, commerce_product_line_item_types());

		if ($quantity > 0) {

			// Firstly, we need to note the "checkout complete" so that rules can do its work
			$order = commerce_order_status_update($order, 'checkout_complete');
			commerce_checkout_complete($order);
		  	watchdog('commerce_iveri_lite', 'Checkout process complete for order ID: !order_id', array('!order_id' => $order_id));

			$transaction = NULL;

			if($commerce_transaction_id) {
				$transaction = commerce_payment_transaction_load($commerce_transaction_id);
				 watchdog('commerce_iveri_lite', 'Loaded transaction !transaction_id',array('!transaction_id' => $transaction->transaction_id));
			}  
			
			if(!$transaction) {
		  		watchdog('commerce_iveri_lite', 'Creating a new transaction because an existing one could not be found for order ID: !order_id', array('!order_id' => $order_id));
				// Prepare a transaction object to log the API response.
				$transaction = commerce_payment_transaction_new('iveri_lite', $order->order_id);
				$transaction->instance_id = $payment_method['instance_id'];
				$transaction->amount = $amount;
				$transaction->currency_code = IVERI_CURRENCY_CODE;
			}

			$transaction->remote_id = $transaction_index;
			$transaction->remote_status = $auth_type;
			$transaction->payload[REQUEST_TIME] = $_POST;

			$message = t('Paid by iVeri Lite order with auth code: !auth_code',
					array(
						'!auth_code' => $transaction_index,
					     )
				    );

			$transaction->message = $message;
			$message = '';
			$transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
			commerce_payment_transaction_save($transaction);
			commerce_payment_redirect_pane_next_page($order);
    			drupal_goto('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key']);

		}
		else 
		{ // order is invalid or empty, or might be a duplicate SIM response
		  $message = t("We're sorry, but due to a technical problem, your order could not be processed. Please contact our help center.");
		  watchdog('commerce_iveri_lite', 'An invalid or empty order made it to checkout.  Order ID: !order_id', array('!order_id' => $order_id), WATCHDOG_ERROR);
		}
	}

	if($message) {
	  drupal_set_message($message, array() , WATCHDOG_ERROR);
	}

	drupal_goto('cart');
}

/**
 * Callback from iVeri - NOK
 */
function commerce_iveri_lite_iver_callback_nok() {
  commerce_iveri_lite_iver_callback_eok('failed');
}

/**
 * Callback from iVeri - TOK
 */
function commerce_iveri_lite_iver_callback_tok() {
	commerce_iveri_lite_iver_callback_eok('tryagain');
}

/**
 * Callback from iVeri - EOK
 */
function commerce_iveri_lite_iver_callback_eok($type = 'error') {

	$result_description = $_POST['LITE_RESULT_DESCRIPTION'];
	$order_id = $_POST['ECOM_CONSUMERORDERID'];
	$payment_id = $_POST['DC_PAYMENT_ID'];
	$commerce_transaction_id = $_POST['DC_TRANSACTION_ID'];

	watchdog('commerce_iveri_lite', 'Receiving !type payment response for order !order_id from iVeri Lite', array('!order_id' => $order_id,'!type' => $type),WATCHDOG_WARNING);

	$order = commerce_order_load($order_id);
	$message = $result_description;
	if($order) {
		if($commerce_transaction_id) {
			$transaction = commerce_payment_transaction_load($commerce_transaction_id);
			watchdog('commerce_iveri_lite', 'Loaded transaction !transaction_id',array('!transaction_id' => $transaction->transaction_id));
			if($transaction) {	
				$transaction->message = 'Failed: ['.$type.'] ' . $result_description;
				$transaction->status = COMMERCE_PAYMENT_STATUS_FAILURE;
				commerce_payment_transaction_save($transaction);
			}
		}  
		drupal_set_message('Transaction declined ['.$type.']: ' . $message,'error payment');
		commerce_payment_redirect_pane_previous_page($order); 
		drupal_goto('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key']);
	} else {
		drupal_set_message('Transaction declined ['.$type.']: ' . $message,'error payment');
		drupal_goto('cart');
	}
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_iveri_lite_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['commerce_iveri_lite'] = array(
    'title' => t('Credit Card'),
    'description' => t('Process payments using the iVeri payment gateway (Lite)'),
    'terminal' => FALSE,
    'active' => TRUE,
    'offsite' => TRUE,
    'offsite_autoredirect' => FALSE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: settings form.
 */
function commerce_iveri_lite_settings_form($settings = NULL) {
  $form = array();

  $settings = (array) $settings + array(
    'test_key' => '',
    'live_key' => '',
    'submission_endpoint' => empty($settings['submission_endpoint']) ? IVERI_LITE_SUBMISSION_ENDPOINT : $settings['submission_endpoint'],
    'auth_info_endpoint' => empty($settings['auth_info_endpoint']) ? IVERI_LITE_AUTH_INFO_ENDPOINT : $settings['auth_info_endpoint'],
    'redirect_validation_hash' => empty($settings['redirect_validation_hash']) ?
      _commerce_iveri_lite_randomstring(16) : $settings['redirect_validation_hash'],
    'transaction_mode' => 'test',
  );

  $form['test_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Test Key'),
    '#description' => t('Your test application key.'),
    '#default_value' => $settings['test_key'],
  );

  $form['live_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API Live Key'),
    '#description' => t('Your live application key.'),
    '#default_value' => $settings['live_key'],
  );

  $form['submission_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('API Submission Endpoint'),
    '#description' => t('Submission Endpoint (You\'ll probably not want to mess with this)'),
    '#default_value' => $settings['submission_endpoint'],
  );

  $form['auth_info_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('API Authorization Endpoint'),
    '#description' => t('Authorization Endpoint (You\'ll probably not want to mess with this either)'),
    '#default_value' => $settings['auth_info_endpoint'],
  );

  $form['redirect_validation_hash'] = array(
    '#type' => 'textfield',
    '#title' => t('Redirect validation key'),
    '#default_value' => $settings['redirect_validation_hash'],
    '#description' => t('An MD5 hash key for validating redirect responses.  A random key has been generated for your convenience.  Do not leave this blank!'),
  );

  $form['transaction_mode'] = array(
    '#type' => 'select',
    '#title' => t('Transaction mode'),
    '#description' => t('Test is development server, live will process transactions'),
    '#options' => array(
      'test' => t('Test - Development Mode'),
      'live' => t('Live - Production Mode'),
    ),
    '#multiple' => FALSE,
    '#default_value' => $settings['transaction_mode'],
  );
  
  return $form;
}

/**
 * Payment method callback: submit form.
 */
function commerce_iveri_lite_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');

  $fields = array(
    'type' => array(
      'visa',
      'mastercard',
      'discover',
      'amex',
    ),
    'code' => 'CVV Code',
    'owner' => 'Card Holder\'s Name'
  );

  $default = array(
    'number' => '4242424242424242',
  );

  return commerce_payment_credit_card_form($fields, $default);
}

/**
 * Payment method callback: submit form validation.
 */
function commerce_iveri_lite_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  // Validate the credit card fields.
  module_load_include('inc', 'commerce_payment', 'includes/commerce_payment.credit_card');
  $settings = array(
    'form_parents' => array_merge($form_parents, array('credit_card')),
  );

  if (!commerce_payment_credit_card_validate($pane_values['credit_card'], $settings)) {
    return FALSE;
  }
}

/**
 * Payment method callback: submit form submission.
 */
function commerce_iveri_lite_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  $order->data['commerce_iveri_lite'] = $pane_values;

  $transaction = commerce_iveri_lite_transaction($payment_method, $order, $charge);

  if($transaction) {
    $order->data['commerce_iveri_lite']['transaction_id'] = $transaction->transaction_id;
  }

}

/**
 * Creates an example payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 * @param $name
 *   The name entered on the submission form.
 */
function commerce_iveri_lite_transaction($payment_method, $order, $charge) {
  $transaction = commerce_payment_transaction_new('iveri_lite', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_PENDING;

  commerce_payment_transaction_save($transaction);

  watchdog('commerce_iveri_lite', 'Created transaction !transaction_id for order !order_id',array('!transaction_id' => $transaction->transaction_id,'!order_id' => $order->order_id));
  return $transaction;
}


/**
 * Redirect form to iVeri Service
 */
function commerce_iveri_lite_redirect_form($form, &$form_state, $order, $payment_method) {
  // Return an error if the enabling action's settings haven't been configured.
  if (
	($payment_method['settings']['transaction_mode'] == 'test' && empty($payment_method['settings']['test_key'])) 
	|| ($payment_method['settings']['transaction_mode'] == 'live' && empty($payment_method['settings']['live_key'])) 
  ) {
    drupal_set_message(t('iVeri Lite is not configured for use. Please choose a processing mode, and set your application keys.'), 'error');
    return array();
  }

  $form['#action'] = $payment_method['settings']['submission_endpoint'];

  _commerce_iveri_lite_redirect_form_populate($form, $form_state, $order, $payment_method);
  
  $cancel_url = check_url(url('checkout/' . $order->order_id . '/payment/back/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)));
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit Order'),
    '#suffix' => '&nbsp;&nbsp;<a href="' . $cancel_url . '">Cancel</a>',
  );

  return $form; 

}

/**
 * Function to populate a form for iVeri Lite Submission based on an order
 */
/**
 * Redirect form to iVeri Service
 */
function _commerce_iveri_lite_redirect_form_populate(&$form, &$form_state, $order, $payment_method) {

	$order_wrapper = entity_metadata_wrapper('commerce_order', $order);

	$form['Lite_Merchant_ApplicationID'] = array(
		'#type' => 'hidden',
		'#value' => ($payment_method['settings']['transaction_mode']=='live') ? $payment_method['settings']['live_key'] : $payment_method['settings']['test_key'],
	);

	$form['Lite_Order_Amount'] = array(
		'#type' => 'hidden',
		'#value' => $order_wrapper->commerce_order_total->amount->value()
	);

	$form['Lite_Order_Terminal'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Lite_Order_AuthorisationCode'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Lite_Order_BudgetPeriod'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Lite_Website_TextColor'] = array(
		'#type' => 'hidden',
		'#value' => empty($payment_method['settings']['website_text_color']) ? '#000000' : $payment_method['settings']['website_text_color'],
	);

	$form['Lite_Website_BGColor'] = array(
		'#type' => 'hidden',
		'#value' => empty($payment_method['settings']['website_bg_color']) ? '#ffffff' : $payment_method['settings']['website_bg_color'],
	);

	$form['Lite_AutoInvoice_Ext'] = array(
		'#type' => 'hidden',
		'#value' => 'AUT',
	);

	$form['Lite_On_Error_Resume_Next'] = array(
		'#type' => 'hidden',
		'#value' => 'true',
	);

	$form['DC_PAYMENT_ID'] = array(
		'#type' => 'hidden', 
		'#value' => $payment_method['instance_id']
	);

	$form['DC_TRANSACTION_ID'] = array(
		'#type' => 'hidden', 
		'#value' => $order->data['commerce_iveri_lite']['transaction_id']
	);

	$cnt = 1;
	$discount = 0;
	foreach ($order_wrapper->commerce_line_items as $delta => $line_item_wrapper) {

		if($line_item_wrapper->commerce_unit_price->amount->value() < 0) {
		  $discount += abs(intval($line_item_wrapper->quantity->value()) * $line_item_wrapper->commerce_unit_price->amount->value());
		  continue;
		}

		$li_val = '['.$line_item_wrapper->line_item_label->value() .']';

		if(in_array('commerce_product',array_keys($line_item_wrapper->getPropertyInfo()))) {
			$commerce_product_wrapper = $line_item_wrapper->commerce_product;
			if($commerce_product_wrapper && $commerce_product_wrapper->product_id && $commerce_product_wrapper->product_id->value()) {
				$li_val .= $commerce_product_wrapper->title->value();
			}
		}
		
		$form['Lite_Order_LineItems_Product_' . $cnt] = array(
				'#type' => 'hidden',
				'#value' => $li_val,
				);

		$form['Lite_Order_LineItems_Quantity_' . $cnt] = array(
				'#type' => 'hidden',
				'#value' => intval($line_item_wrapper->quantity->value()),
				);

		$form['Lite_Order_LineItems_Amount_' . $cnt] = array(
				'#type' => 'hidden',
				'#value' => $line_item_wrapper->commerce_unit_price->amount->value(),
				);

		file_put_contents('/tmp/bgout.line', print_r($line_item_wrapper, TRUE));
	}

	if($discount > 0) {
		$form['Lite_Order_DiscountAmount'] = array(
				'#type' => 'hidden',
				'#value' => $discount
				);
	}

	$form['Lite_Website_Successful_url'] = array(
		'#type' => 'hidden',
		'#value' => url('iveri/lite/back/ok',array('absolute' => TRUE)),
	);

	$form['Lite_Website_Fail_url'] = array(
		'#type' => 'hidden',
		'#value' => url('iveri/lite/back/nok',array('absolute' => TRUE)),
	);

	$form['Lite_Website_TryLater_url'] = array(
		'#type' => 'hidden',
		'#value' => url('iveri/lite/back/tok',array('absolute' => TRUE)),
	);

	$form['Lite_Website_Error_url'] = array(
		'#type' => 'hidden',
		'#value' => url('iveri/lite/back/eok',array('absolute' => TRUE)),
	);

 	/* Leaving out shipping details for now */
	// TODO: Fill in the postal details if some setting is true
	$form['Ecom_ShipTo_Postal_Name_Prefix'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_Name_First'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_Name_Middle'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_Name_Last'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_Name_Suffix'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_Street_Line1'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_Street_Line2'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_Street_Line3'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_City'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_StateProv'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_PostalCode'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Postal_CountryCode'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Telecom_Phone_Number'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_ShipTo_Online_Email'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	// Bill to details
	// TODO: Fill in the recipients details
	$form['Ecom_BillTo_Postal_Name_Prefix'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_Name_First'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_Name_Middle'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_Name_Last'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_Name_Suffix'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_Street_Line1'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_Street_Line2'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_Street_Line3'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_City'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_StateProv'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_PostalCode'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Postal_CountryCode'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Telecom_Phone_Number'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_BillTo_Online_Email'] = array(
		'#type' => 'hidden',
		'#value' => $order->mail,
	);

	/////////////////////////////////////////////////////////////////////////////
	// Card details
	$form['Ecom_Payment_Card_Name'] = array(
		'#type' => 'hidden',
		'#value' => $order->data['commerce_iveri_lite']['credit_card']['owner'],
	);

	$cc_type = strtoupper(substr($order->data['commerce_iveri_lite']['credit_card']['type'],0,4));

	$form['Ecom_Payment_Card_Type'] = array(
		'#type' => 'hidden',
		'#value' => $cc_type,
	);

	$form['Ecom_Payment_Card_Number'] = array(
		'#type' => 'hidden',
		'#value' => $order->data['commerce_iveri_lite']['credit_card']['number'],
	);

	$form['Ecom_Payment_Card_Verification'] = array(
		'#type' => 'hidden',
		'#value' => $order->data['commerce_iveri_lite']['credit_card']['code'],
	);

	$form['Ecom_Payment_Card_Protocols'] = array(
		'#type' => 'hidden',
		'#value' => 'iVeri',
	);

	$form['Ecom_Payment_Card_StartDate_Day'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_Payment_Card_StartDate_Month'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_Payment_Card_StartDate_Year'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_Payment_Card_ExpDate_Day'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_Payment_Card_ExpDate_Month'] = array(
		'#type' => 'hidden',
		'#value' => $order->data['commerce_iveri_lite']['credit_card']['exp_month'],
	);

	$form['Ecom_Payment_Card_ExpDate_Year'] = array(
		'#type' => 'hidden',
		'#value' => $order->data['commerce_iveri_lite']['credit_card']['exp_year'],
	);

	// Final order data
	$form['Ecom_ConsumerOrderID'] = array(
		'#type' => 'hidden',
		'#value' => $order->order_id,
	);

	$form['LITE_CONSUMERORDERID_PREFIX'] = array(
		'#type' => 'hidden',
		'#value' => 'DC',
	);

	$form['Ecom_SchemaVersion'] = array(
		'#type' => 'hidden',
		'#value' => '',
	);

	$form['Ecom_TransactionComplete'] = array(
		'#type' => 'hidden',
		'#value' => 'false',
	);

	$form['Lite_Payment_Card_PreAuthMode'] = array(
		'#type' => 'hidden',
		'#value' => 'false',
	);
/*
  file_put_contents('/tmp/bgout.form', print_r($form, TRUE));
  file_put_contents('/tmp/bgout.formstate', print_r($form_state, TRUE));
  file_put_contents('/tmp/bgout.order', print_r($order, TRUE));
  file_put_contents('/tmp/bgout.order_wrapper', print_r($order_wrapper, TRUE));
  file_put_contents('/tmp/bgout.payment', print_r($payment_method, TRUE));*/
}


/**
 * Generate a random string (from DrupalTestCase::randomString).
 */
function _commerce_iveri_lite_randomstring($length = 8) {
  $str = '';

  for ($i = 0; $i < $length; $i++) {
    $str .= chr(mt_rand(32, 126));
  }

  return $str;
}

